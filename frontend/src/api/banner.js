import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getBannerList(params) {
  return request({
    url: api_prefix + 'banner/list',
    method: 'get',
    params
  })
}

export function getBannerByID(id) {
  return request({
    url: api_prefix + 'banner/get_id',
    method: 'get',
    params: { id }
  })
}

export function getBannerCountByPosID(pos_id) {
  return request({
    url: api_prefix + 'banner/get_banner_count',
    method: 'get',
    params: { pos_id }
  })
}

export function addBanner(data) {
  return request({
    url: api_prefix + 'banner/add',
    method: 'post',
    data
  })
}

export function editBanner(data) {
  return request({
    url: api_prefix + 'banner/edit',
    method: 'put',
    data
  })
}

export function updateBanner(data) {
  return request({
    url: api_prefix + 'banner/update',
    method: 'put',
    data
  })
}

export function deleteBanner(ids) {
  return request({
    url: api_prefix + 'banner/delete',
    method: 'delete',
    data: { ids }
  })
}
