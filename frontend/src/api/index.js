import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getDefaultConfig() {
  return request({
    url: api_prefix + 'index/default_config',
    method: 'get'
  })
}
