/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : tn_website_open

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 16/09/2020 14:21:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tn_admin
-- ----------------------------
DROP TABLE IF EXISTS `tn_admin`;
CREATE TABLE `tn_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员主键id',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '登陆用户名',
  `password` varchar(80) NOT NULL DEFAULT '' COMMENT '登陆密码',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '管理员性别(0 男 1 女)',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员头像',
  `nick_name` varchar(20) NOT NULL DEFAULT '' COMMENT '管理员别名',
  `introduction` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员简介',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '管理员开启状态',
  `role_id` int(255) NOT NULL DEFAULT '0' COMMENT '所属角色的id',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='保存管理员信息';

-- ----------------------------
-- Records of tn_admin
-- ----------------------------
BEGIN;
INSERT INTO `tn_admin` VALUES (1, 'super_admin', '$2y$10$wA6dzizrbIUsdEhCb9vVtuC9zAQOubCNisdR7yG0C5L2oXU3XdZzO', 0, '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', '超级管理员', '图鸟超级管理员', 1, 1, 1588213740, 1600237135, NULL);
INSERT INTO `tn_admin` VALUES (2, 'admin', '$2y$10$.LopVHxUhiQ0kAI/Gnj2qOvbhnnVJmYi36ev0eWm97UHrAKCb4C.q', 0, '/storage/uploads/img_list/27/5c2e56b7b0967f2a52ad19fe0bb765.jpg', '图鸟管理员', '普通管理员', 0, 2, 1588215393, 1591597121, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_atlas
-- ----------------------------
DROP TABLE IF EXISTS `tn_atlas`;
CREATE TABLE `tn_atlas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '图集主键id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '图片的名称，对应上传文件的原名称',
  `img_path` varchar(255) NOT NULL DEFAULT '' COMMENT '图片保存的路径',
  `md5` varchar(34) NOT NULL DEFAULT '' COMMENT '记录图片的md5值，用于去重复',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '图片的保存类型（1、保存在本地）',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片所属分类',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name_md5_category` (`name`,`md5`,`category_id`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='保存图集的图片信息';

-- ----------------------------
-- Records of tn_atlas
-- ----------------------------
BEGIN;
INSERT INTO `tn_atlas` VALUES (1, 'logo.png', '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', 'c6aab26376f3f6dc3c5a737784e45985', 1, 1, 1590207902, 1590207902, NULL);
INSERT INTO `tn_atlas` VALUES (2, 'logo.jpg', '/storage/uploads/img_list/27/5c2e56b7b0967f2a52ad19fe0bb765.jpg', '275c2e56b7b0967f2a52ad19fe0bb765', 1, 2, 1590207934, 1590207934, NULL);
INSERT INTO `tn_atlas` VALUES (3, 'spring.jpg', '/storage/uploads/img_list/28/d181dcfaa45cbbdd51c2b2e4a9379d.jpg', '28d181dcfaa45cbbdd51c2b2e4a9379d', 1, 4, 1590457210, 1590457210, NULL);
INSERT INTO `tn_atlas` VALUES (4, 'summer.jpg', '/storage/uploads/img_list/52/1648532c5e5f133243d3345cce55f8.jpg', '521648532c5e5f133243d3345cce55f8', 1, 4, 1590457829, 1590457829, NULL);
INSERT INTO `tn_atlas` VALUES (5, 'autumn.jpg', '/storage/uploads/img_list/f1/f3adf879a21152d4651985df31a24c.jpg', 'f1f3adf879a21152d4651985df31a24c', 1, 3, 1590931739, 1590931739, NULL);
INSERT INTO `tn_atlas` VALUES (6, 'b47753a0-58db-4bf3-a97b-340ddba70eef.jpeg', '/storage/uploads/img_list/95/a5526255d95418b434964c52cd056d.jpeg', '95a5526255d95418b434964c52cd056d', 1, 4, 1600235703, 1600235703, NULL);
INSERT INTO `tn_atlas` VALUES (7, '924619aa-c0f9-426e-8ba6-b7392fbe53c8.jpeg', '/storage/uploads/img_list/96/dbcde963e2a4d86fd460b6e4bf1769.jpeg', '96dbcde963e2a4d86fd460b6e4bf1769', 1, 4, 1600235703, 1600235703, NULL);
INSERT INTO `tn_atlas` VALUES (8, '8c310851-15a3-4f48-a82c-9d7a1be19c33.jpeg', '/storage/uploads/img_list/1d/f27f460d2c5c9c0272324a9a9e5718.jpeg', '1df27f460d2c5c9c0272324a9a9e5718', 1, 4, 1600235703, 1600235703, NULL);
INSERT INTO `tn_atlas` VALUES (9, 'db11b70f-0b2c-4b20-9e03-b650d7f0fe06.png', '/storage/uploads/img_list/96/00a3c9fce9978d9e767f2e2f90fc7b.png', '9600a3c9fce9978d9e767f2e2f90fc7b', 1, 4, 1600235704, 1600235704, NULL);
INSERT INTO `tn_atlas` VALUES (10, 'logo2 (1).jpg', '/storage/uploads/img_list/0c/2683c1e4d721f44aa466869a4c9d11.jpg', '0c2683c1e4d721f44aa466869a4c9d11', 1, 6, 1600236309, 1600236309, NULL);
INSERT INTO `tn_atlas` VALUES (11, '图鸟-关于我们new.png', '/storage/uploads/img_list/58/48db7300add3602fc43156d305aae8.png', '5848db7300add3602fc43156d305aae8', 1, 6, 1600236309, 1600236309, NULL);
INSERT INTO `tn_atlas` VALUES (12, '1111 (1).png', '/storage/uploads/img_list/63/3896e9d9cff93d354ee6c1419e6516.png', '633896e9d9cff93d354ee6c1419e6516', 1, 6, 1600236462, 1600236462, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_atlas_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_atlas_category`;
CREATE TABLE `tn_atlas_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '图集类目主键id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '图集类目名称',
  `sort` mediumint(8) unsigned NOT NULL DEFAULT '1' COMMENT '图集排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启图集',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='保存图集类目的相关信息';

-- ----------------------------
-- Records of tn_atlas_category
-- ----------------------------
BEGIN;
INSERT INTO `tn_atlas_category` VALUES (1, '图鸟官方图标', 1, 1, 1589794899, 1589797112, NULL);
INSERT INTO `tn_atlas_category` VALUES (2, '头像', 2, 1, 1589797169, 1589797169, NULL);
INSERT INTO `tn_atlas_category` VALUES (3, '杂项', 3, 1, 1590125624, 1590125624, NULL);
INSERT INTO `tn_atlas_category` VALUES (4, '轮播图', 4, 1, 1590457188, 1590457188, NULL);
INSERT INTO `tn_atlas_category` VALUES (5, '内容主图', 5, 1, 1592025864, 1592025864, NULL);
INSERT INTO `tn_atlas_category` VALUES (6, '小程序设置', 6, 1, 1592025878, 1592025878, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_menu`;
CREATE TABLE `tn_auth_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色菜单主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色菜单上级id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '角色菜单路由名字',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '角色菜单侧边栏喝面包屑展示的名称',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '角色菜单路由地址',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '角色菜单文件所在路径（为空则为Layout）',
  `redirect` varchar(255) NOT NULL DEFAULT '' COMMENT '角色菜单重定向地址',
  `icon` varchar(80) NOT NULL DEFAULT '' COMMENT '角色菜单的图标',
  `is_link` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否为链接',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否隐藏不显示在侧边栏（1 隐藏 0 显示）',
  `is_click_in_breadcrumb` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '标记角色菜单是否可以在面包屑中点击',
  `always_show` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否总是显示在侧边栏（1 永远显示 0 不显示）',
  `no_cache` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否被缓存（1 缓存 0 不缓存）',
  `breadcrumb_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '标记角色菜单是否显示在面包屑（1 显示在面包屑 0 不显示在面包屑）',
  `public_menu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '标记角色菜单是否为公共菜单',
  `sort` mediumint(8) unsigned NOT NULL DEFAULT '1' COMMENT '角色菜单排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '角色菜单状态',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COMMENT='保存角色菜单的数据';

-- ----------------------------
-- Records of tn_auth_menu
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_menu` VALUES (1, 0, 'Admin', '管理员', '/admin', '', '/admin/admin/list', 'tn_admin', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588504538, 1588599355, NULL);
INSERT INTO `tn_auth_menu` VALUES (2, 1, 'Admin-Admin', '管理员人员管理', 'admin', 'admin/admin/index', '/admin/admin/list', 'tn_admin_1', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588560688, 1589782361, NULL);
INSERT INTO `tn_auth_menu` VALUES (3, 2, 'Admin-Admin-List', '人员列表', 'list', 'admin/admin/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588562226, 1588599355, NULL);
INSERT INTO `tn_auth_menu` VALUES (4, 1, 'Admin-Auth', '管理员权限管理', 'auth', 'admin/auth/index', '', 'tn_admin_auth', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588570663, 1588667986, NULL);
INSERT INTO `tn_auth_menu` VALUES (5, 4, 'Admin-Auth-Role-List', '角色列表', 'role', 'admin/auth/role/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588570730, 1588667986, NULL);
INSERT INTO `tn_auth_menu` VALUES (6, 4, 'Admin-Auth-Menu-List', '菜单列表', 'menu', 'admin/auth/menu/list', '', '', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1588570858, 1588860419, NULL);
INSERT INTO `tn_auth_menu` VALUES (7, 0, 'Tuniao', '图鸟官网', 'https://tuniaokj.com/', '', '', 'international', 1, 0, 1, 0, 0, 1, 1, 10, 1, 1588571120, 1590375700, NULL);
INSERT INTO `tn_auth_menu` VALUES (8, 4, 'Admin-Auth-Rule-List', '规则列表', 'rule', 'admin/auth/rule/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588852174, 1588860422, NULL);
INSERT INTO `tn_auth_menu` VALUES (9, 0, 'System', '系统', '/system', '', '', 'xitongshezhi', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588916619, 1589505965, NULL);
INSERT INTO `tn_auth_menu` VALUES (10, 9, 'SystemConfig-List', '配置项列表', 'config-list', 'system-config/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588916672, 1589505986, NULL);
INSERT INTO `tn_auth_menu` VALUES (11, 9, 'SystemConfig-Menu', '系统配置', 'menu', 'system-config/menu', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1589015089, 1589015089, NULL);
INSERT INTO `tn_auth_menu` VALUES (12, 9, 'SystemLog-List', '系统日志', 'log-list', 'system-log/list', '', '', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1589505946, 1589506013, NULL);
INSERT INTO `tn_auth_menu` VALUES (13, 0, 'Atlas', '图集', '/atlas', '', '/atlas/category', 'table', 0, 0, 1, 0, 0, 1, 1, 3, 1, 1589782312, 1589782312, NULL);
INSERT INTO `tn_auth_menu` VALUES (14, 13, 'Altas-Category', '图集分类', 'category', 'atlas/category', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1589783179, 1589783179, NULL);
INSERT INTO `tn_auth_menu` VALUES (15, 13, 'Atlas-List', '图集列表', 'list', 'atlas/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590039557, 1590039557, NULL);
INSERT INTO `tn_auth_menu` VALUES (16, 0, 'WeChat', '微信', '/wechat', '', '', 'wechat', 0, 0, 1, 0, 0, 1, 0, 10, 1, 1590375766, 1591580308, NULL);
INSERT INTO `tn_auth_menu` VALUES (17, 16, 'WeChat-Config', '配置管理', 'config', 'wechat/config/index', '/wechat/config/menu', 'config', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590376554, 1590478828, NULL);
INSERT INTO `tn_auth_menu` VALUES (18, 17, 'WeChat-Config-List', '微信配置项列表', 'list', 'wechat/config/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590377821, 1590377821, NULL);
INSERT INTO `tn_auth_menu` VALUES (19, 17, 'WeChat-Config-Menu', '微信配置', 'menu', 'wechat/config/menu', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590377865, 1590377865, NULL);
INSERT INTO `tn_auth_menu` VALUES (20, 0, 'Banner', '轮播', '/banner', '', '/banner/banner-list', 'banner', 0, 0, 1, 0, 0, 1, 0, 4, 1, 1590401099, 1590455972, NULL);
INSERT INTO `tn_auth_menu` VALUES (21, 20, 'Banner-Pos', '轮播位管理', 'banner-pos', 'banner/banner-pos/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590401219, 1590401219, NULL);
INSERT INTO `tn_auth_menu` VALUES (22, 20, 'Banner-List', '轮播图管理', 'banner-list', 'banner/banner/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590455952, 1590455952, NULL);
INSERT INTO `tn_auth_menu` VALUES (23, 16, 'WeChat-User', '用户管理', 'user', 'wechat/user/list', '', 'peoples', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590478824, 1590478958, NULL);
INSERT INTO `tn_auth_menu` VALUES (24, 0, 'Category', '栏目', '/category', '', '/category/list', 'category', 0, 0, 1, 0, 0, 1, 0, 5, 1, 1590491178, 1590491178, NULL);
INSERT INTO `tn_auth_menu` VALUES (25, 24, 'Category-List', '栏目管理', 'list', 'category/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590491215, 1590491215, NULL);
INSERT INTO `tn_auth_menu` VALUES (26, 0, 'Business', '业务', '/business', '', '/business/list', 'business', 0, 0, 1, 0, 0, 1, 0, 6, 1, 1590560214, 1590560268, NULL);
INSERT INTO `tn_auth_menu` VALUES (27, 26, 'Business-List', '业务列表', 'list', 'business/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590560254, 1590560254, NULL);
INSERT INTO `tn_auth_menu` VALUES (28, 0, 'ModelTable', '模型', '/model-table', '', '/model-table/list', 'model', 0, 0, 1, 0, 0, 1, 0, 7, 1, 1590668835, 1590668914, NULL);
INSERT INTO `tn_auth_menu` VALUES (29, 28, 'ModelTable-List', '模型列表', 'list', 'model-table/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590668879, 1590668879, NULL);
INSERT INTO `tn_auth_menu` VALUES (30, 0, 'Content', '内容', '/content', '', '/content/list', 'content', 0, 0, 1, 0, 0, 1, 0, 8, 1, 1590819770, 1590819776, NULL);
INSERT INTO `tn_auth_menu` VALUES (31, 30, 'Content-List', '内容管理', 'list', 'content/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590819822, 1590819822, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role`;
CREATE TABLE `tn_auth_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限角色主键id',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '角色标题',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '角色状态（1 打开 ；0 关闭）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `role_title_name` (`title`,`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='保存用户权限角色信息';

-- ----------------------------
-- Records of tn_auth_role
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role` VALUES (1, '超级管理员', 'super_admin', 1, 1586937088, 1600235603, NULL);
INSERT INTO `tn_auth_role` VALUES (2, '管理员', 'admin', 0, 1586937444, 1600235615, NULL);
INSERT INTO `tn_auth_role` VALUES (3, '访客', 'guest', 0, 1588815255, 1600235625, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role_half_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_half_menu`;
CREATE TABLE `tn_auth_role_half_menu` (
  `role_id` int(10) unsigned NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '菜单id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存菜单半选节点id信息';

-- ----------------------------
-- Records of tn_auth_role_half_menu
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_menu`;
CREATE TABLE `tn_auth_role_menu` (
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `menu_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '菜单id',
  UNIQUE KEY `role_menu_id` (`role_id`,`menu_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `menu_id` (`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存角色与菜单之间的数据';

-- ----------------------------
-- Records of tn_auth_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role_menu` VALUES (1, 1);
INSERT INTO `tn_auth_role_menu` VALUES (1, 2);
INSERT INTO `tn_auth_role_menu` VALUES (1, 3);
INSERT INTO `tn_auth_role_menu` VALUES (1, 4);
INSERT INTO `tn_auth_role_menu` VALUES (1, 5);
INSERT INTO `tn_auth_role_menu` VALUES (1, 6);
INSERT INTO `tn_auth_role_menu` VALUES (1, 7);
INSERT INTO `tn_auth_role_menu` VALUES (1, 8);
INSERT INTO `tn_auth_role_menu` VALUES (1, 9);
INSERT INTO `tn_auth_role_menu` VALUES (1, 10);
INSERT INTO `tn_auth_role_menu` VALUES (1, 11);
INSERT INTO `tn_auth_role_menu` VALUES (1, 12);
INSERT INTO `tn_auth_role_menu` VALUES (1, 13);
INSERT INTO `tn_auth_role_menu` VALUES (1, 14);
INSERT INTO `tn_auth_role_menu` VALUES (1, 15);
INSERT INTO `tn_auth_role_menu` VALUES (1, 16);
INSERT INTO `tn_auth_role_menu` VALUES (1, 17);
INSERT INTO `tn_auth_role_menu` VALUES (1, 18);
INSERT INTO `tn_auth_role_menu` VALUES (1, 19);
INSERT INTO `tn_auth_role_menu` VALUES (1, 20);
INSERT INTO `tn_auth_role_menu` VALUES (1, 21);
INSERT INTO `tn_auth_role_menu` VALUES (1, 22);
INSERT INTO `tn_auth_role_menu` VALUES (1, 23);
INSERT INTO `tn_auth_role_menu` VALUES (1, 24);
INSERT INTO `tn_auth_role_menu` VALUES (1, 25);
INSERT INTO `tn_auth_role_menu` VALUES (1, 26);
INSERT INTO `tn_auth_role_menu` VALUES (1, 27);
INSERT INTO `tn_auth_role_menu` VALUES (1, 28);
INSERT INTO `tn_auth_role_menu` VALUES (1, 29);
INSERT INTO `tn_auth_role_menu` VALUES (1, 30);
INSERT INTO `tn_auth_role_menu` VALUES (1, 31);
INSERT INTO `tn_auth_role_menu` VALUES (2, 1);
INSERT INTO `tn_auth_role_menu` VALUES (2, 2);
INSERT INTO `tn_auth_role_menu` VALUES (2, 3);
INSERT INTO `tn_auth_role_menu` VALUES (2, 4);
INSERT INTO `tn_auth_role_menu` VALUES (2, 5);
INSERT INTO `tn_auth_role_menu` VALUES (2, 6);
INSERT INTO `tn_auth_role_menu` VALUES (2, 7);
INSERT INTO `tn_auth_role_menu` VALUES (2, 8);
INSERT INTO `tn_auth_role_menu` VALUES (2, 13);
INSERT INTO `tn_auth_role_menu` VALUES (2, 14);
INSERT INTO `tn_auth_role_menu` VALUES (2, 15);
INSERT INTO `tn_auth_role_menu` VALUES (3, 1);
INSERT INTO `tn_auth_role_menu` VALUES (3, 2);
INSERT INTO `tn_auth_role_menu` VALUES (3, 3);
INSERT INTO `tn_auth_role_menu` VALUES (3, 4);
INSERT INTO `tn_auth_role_menu` VALUES (3, 5);
INSERT INTO `tn_auth_role_menu` VALUES (3, 6);
INSERT INTO `tn_auth_role_menu` VALUES (3, 7);
INSERT INTO `tn_auth_role_menu` VALUES (3, 8);
INSERT INTO `tn_auth_role_menu` VALUES (3, 13);
INSERT INTO `tn_auth_role_menu` VALUES (3, 14);
INSERT INTO `tn_auth_role_menu` VALUES (3, 15);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_rule`;
CREATE TABLE `tn_auth_role_rule` (
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `rule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限id',
  UNIQUE KEY `role_rule_id` (`role_id`,`rule_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `rule_id` (`rule_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存权限角色和权限规则之间的关系';

-- ----------------------------
-- Records of tn_auth_role_rule
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role_rule` VALUES (1, 2);
INSERT INTO `tn_auth_role_rule` VALUES (1, 3);
INSERT INTO `tn_auth_role_rule` VALUES (1, 4);
INSERT INTO `tn_auth_role_rule` VALUES (1, 5);
INSERT INTO `tn_auth_role_rule` VALUES (1, 6);
INSERT INTO `tn_auth_role_rule` VALUES (1, 7);
INSERT INTO `tn_auth_role_rule` VALUES (1, 8);
INSERT INTO `tn_auth_role_rule` VALUES (1, 9);
INSERT INTO `tn_auth_role_rule` VALUES (1, 10);
INSERT INTO `tn_auth_role_rule` VALUES (1, 11);
INSERT INTO `tn_auth_role_rule` VALUES (1, 16);
INSERT INTO `tn_auth_role_rule` VALUES (1, 17);
INSERT INTO `tn_auth_role_rule` VALUES (1, 18);
INSERT INTO `tn_auth_role_rule` VALUES (1, 19);
INSERT INTO `tn_auth_role_rule` VALUES (1, 20);
INSERT INTO `tn_auth_role_rule` VALUES (1, 21);
INSERT INTO `tn_auth_role_rule` VALUES (1, 22);
INSERT INTO `tn_auth_role_rule` VALUES (1, 23);
INSERT INTO `tn_auth_role_rule` VALUES (1, 24);
INSERT INTO `tn_auth_role_rule` VALUES (1, 25);
INSERT INTO `tn_auth_role_rule` VALUES (1, 26);
INSERT INTO `tn_auth_role_rule` VALUES (1, 27);
INSERT INTO `tn_auth_role_rule` VALUES (1, 28);
INSERT INTO `tn_auth_role_rule` VALUES (1, 29);
INSERT INTO `tn_auth_role_rule` VALUES (1, 30);
INSERT INTO `tn_auth_role_rule` VALUES (1, 31);
INSERT INTO `tn_auth_role_rule` VALUES (1, 32);
INSERT INTO `tn_auth_role_rule` VALUES (1, 33);
INSERT INTO `tn_auth_role_rule` VALUES (1, 34);
INSERT INTO `tn_auth_role_rule` VALUES (1, 35);
INSERT INTO `tn_auth_role_rule` VALUES (1, 36);
INSERT INTO `tn_auth_role_rule` VALUES (1, 37);
INSERT INTO `tn_auth_role_rule` VALUES (1, 38);
INSERT INTO `tn_auth_role_rule` VALUES (1, 39);
INSERT INTO `tn_auth_role_rule` VALUES (1, 40);
INSERT INTO `tn_auth_role_rule` VALUES (1, 43);
INSERT INTO `tn_auth_role_rule` VALUES (1, 44);
INSERT INTO `tn_auth_role_rule` VALUES (1, 45);
INSERT INTO `tn_auth_role_rule` VALUES (1, 46);
INSERT INTO `tn_auth_role_rule` VALUES (1, 47);
INSERT INTO `tn_auth_role_rule` VALUES (1, 48);
INSERT INTO `tn_auth_role_rule` VALUES (1, 49);
INSERT INTO `tn_auth_role_rule` VALUES (1, 50);
INSERT INTO `tn_auth_role_rule` VALUES (1, 51);
INSERT INTO `tn_auth_role_rule` VALUES (1, 52);
INSERT INTO `tn_auth_role_rule` VALUES (1, 54);
INSERT INTO `tn_auth_role_rule` VALUES (1, 55);
INSERT INTO `tn_auth_role_rule` VALUES (1, 56);
INSERT INTO `tn_auth_role_rule` VALUES (1, 59);
INSERT INTO `tn_auth_role_rule` VALUES (1, 60);
INSERT INTO `tn_auth_role_rule` VALUES (1, 61);
INSERT INTO `tn_auth_role_rule` VALUES (1, 62);
INSERT INTO `tn_auth_role_rule` VALUES (1, 63);
INSERT INTO `tn_auth_role_rule` VALUES (1, 64);
INSERT INTO `tn_auth_role_rule` VALUES (1, 65);
INSERT INTO `tn_auth_role_rule` VALUES (1, 67);
INSERT INTO `tn_auth_role_rule` VALUES (1, 70);
INSERT INTO `tn_auth_role_rule` VALUES (1, 71);
INSERT INTO `tn_auth_role_rule` VALUES (1, 72);
INSERT INTO `tn_auth_role_rule` VALUES (1, 73);
INSERT INTO `tn_auth_role_rule` VALUES (1, 74);
INSERT INTO `tn_auth_role_rule` VALUES (1, 75);
INSERT INTO `tn_auth_role_rule` VALUES (1, 76);
INSERT INTO `tn_auth_role_rule` VALUES (1, 78);
INSERT INTO `tn_auth_role_rule` VALUES (1, 79);
INSERT INTO `tn_auth_role_rule` VALUES (1, 80);
INSERT INTO `tn_auth_role_rule` VALUES (1, 81);
INSERT INTO `tn_auth_role_rule` VALUES (1, 82);
INSERT INTO `tn_auth_role_rule` VALUES (1, 83);
INSERT INTO `tn_auth_role_rule` VALUES (1, 85);
INSERT INTO `tn_auth_role_rule` VALUES (1, 86);
INSERT INTO `tn_auth_role_rule` VALUES (1, 87);
INSERT INTO `tn_auth_role_rule` VALUES (1, 88);
INSERT INTO `tn_auth_role_rule` VALUES (1, 89);
INSERT INTO `tn_auth_role_rule` VALUES (1, 90);
INSERT INTO `tn_auth_role_rule` VALUES (1, 91);
INSERT INTO `tn_auth_role_rule` VALUES (1, 92);
INSERT INTO `tn_auth_role_rule` VALUES (1, 93);
INSERT INTO `tn_auth_role_rule` VALUES (1, 95);
INSERT INTO `tn_auth_role_rule` VALUES (1, 96);
INSERT INTO `tn_auth_role_rule` VALUES (1, 97);
INSERT INTO `tn_auth_role_rule` VALUES (1, 98);
INSERT INTO `tn_auth_role_rule` VALUES (1, 99);
INSERT INTO `tn_auth_role_rule` VALUES (1, 100);
INSERT INTO `tn_auth_role_rule` VALUES (1, 101);
INSERT INTO `tn_auth_role_rule` VALUES (1, 103);
INSERT INTO `tn_auth_role_rule` VALUES (1, 104);
INSERT INTO `tn_auth_role_rule` VALUES (1, 105);
INSERT INTO `tn_auth_role_rule` VALUES (1, 106);
INSERT INTO `tn_auth_role_rule` VALUES (1, 107);
INSERT INTO `tn_auth_role_rule` VALUES (1, 108);
INSERT INTO `tn_auth_role_rule` VALUES (1, 109);
INSERT INTO `tn_auth_role_rule` VALUES (1, 110);
INSERT INTO `tn_auth_role_rule` VALUES (1, 112);
INSERT INTO `tn_auth_role_rule` VALUES (1, 113);
INSERT INTO `tn_auth_role_rule` VALUES (1, 114);
INSERT INTO `tn_auth_role_rule` VALUES (1, 115);
INSERT INTO `tn_auth_role_rule` VALUES (1, 116);
INSERT INTO `tn_auth_role_rule` VALUES (1, 117);
INSERT INTO `tn_auth_role_rule` VALUES (1, 118);
INSERT INTO `tn_auth_role_rule` VALUES (1, 119);
INSERT INTO `tn_auth_role_rule` VALUES (1, 121);
INSERT INTO `tn_auth_role_rule` VALUES (1, 122);
INSERT INTO `tn_auth_role_rule` VALUES (1, 123);
INSERT INTO `tn_auth_role_rule` VALUES (1, 124);
INSERT INTO `tn_auth_role_rule` VALUES (1, 127);
INSERT INTO `tn_auth_role_rule` VALUES (1, 128);
INSERT INTO `tn_auth_role_rule` VALUES (1, 130);
INSERT INTO `tn_auth_role_rule` VALUES (1, 131);
INSERT INTO `tn_auth_role_rule` VALUES (1, 132);
INSERT INTO `tn_auth_role_rule` VALUES (1, 133);
INSERT INTO `tn_auth_role_rule` VALUES (1, 134);
INSERT INTO `tn_auth_role_rule` VALUES (1, 135);
INSERT INTO `tn_auth_role_rule` VALUES (1, 136);
INSERT INTO `tn_auth_role_rule` VALUES (1, 137);
INSERT INTO `tn_auth_role_rule` VALUES (1, 138);
INSERT INTO `tn_auth_role_rule` VALUES (1, 139);
INSERT INTO `tn_auth_role_rule` VALUES (1, 140);
INSERT INTO `tn_auth_role_rule` VALUES (1, 141);
INSERT INTO `tn_auth_role_rule` VALUES (1, 143);
INSERT INTO `tn_auth_role_rule` VALUES (1, 144);
INSERT INTO `tn_auth_role_rule` VALUES (2, 2);
INSERT INTO `tn_auth_role_rule` VALUES (2, 3);
INSERT INTO `tn_auth_role_rule` VALUES (2, 6);
INSERT INTO `tn_auth_role_rule` VALUES (2, 8);
INSERT INTO `tn_auth_role_rule` VALUES (2, 9);
INSERT INTO `tn_auth_role_rule` VALUES (2, 10);
INSERT INTO `tn_auth_role_rule` VALUES (2, 11);
INSERT INTO `tn_auth_role_rule` VALUES (2, 16);
INSERT INTO `tn_auth_role_rule` VALUES (2, 17);
INSERT INTO `tn_auth_role_rule` VALUES (2, 18);
INSERT INTO `tn_auth_role_rule` VALUES (2, 23);
INSERT INTO `tn_auth_role_rule` VALUES (2, 24);
INSERT INTO `tn_auth_role_rule` VALUES (2, 29);
INSERT INTO `tn_auth_role_rule` VALUES (2, 30);
INSERT INTO `tn_auth_role_rule` VALUES (2, 31);
INSERT INTO `tn_auth_role_rule` VALUES (2, 32);
INSERT INTO `tn_auth_role_rule` VALUES (2, 37);
INSERT INTO `tn_auth_role_rule` VALUES (2, 38);
INSERT INTO `tn_auth_role_rule` VALUES (2, 39);
INSERT INTO `tn_auth_role_rule` VALUES (2, 40);
INSERT INTO `tn_auth_role_rule` VALUES (2, 61);
INSERT INTO `tn_auth_role_rule` VALUES (2, 67);
INSERT INTO `tn_auth_role_rule` VALUES (3, 2);
INSERT INTO `tn_auth_role_rule` VALUES (3, 3);
INSERT INTO `tn_auth_role_rule` VALUES (3, 6);
INSERT INTO `tn_auth_role_rule` VALUES (3, 8);
INSERT INTO `tn_auth_role_rule` VALUES (3, 9);
INSERT INTO `tn_auth_role_rule` VALUES (3, 10);
INSERT INTO `tn_auth_role_rule` VALUES (3, 11);
INSERT INTO `tn_auth_role_rule` VALUES (3, 16);
INSERT INTO `tn_auth_role_rule` VALUES (3, 17);
INSERT INTO `tn_auth_role_rule` VALUES (3, 18);
INSERT INTO `tn_auth_role_rule` VALUES (3, 23);
INSERT INTO `tn_auth_role_rule` VALUES (3, 24);
INSERT INTO `tn_auth_role_rule` VALUES (3, 29);
INSERT INTO `tn_auth_role_rule` VALUES (3, 30);
INSERT INTO `tn_auth_role_rule` VALUES (3, 31);
INSERT INTO `tn_auth_role_rule` VALUES (3, 32);
INSERT INTO `tn_auth_role_rule` VALUES (3, 37);
INSERT INTO `tn_auth_role_rule` VALUES (3, 38);
INSERT INTO `tn_auth_role_rule` VALUES (3, 61);
INSERT INTO `tn_auth_role_rule` VALUES (3, 67);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_role_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_user`;
CREATE TABLE `tn_auth_role_user` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限角色id',
  UNIQUE KEY `uid_role_id` (`user_id`,`role_id`) USING BTREE,
  KEY `uid` (`user_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户与权限组之间的关系';

-- ----------------------------
-- Records of tn_auth_role_user
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_role_user` VALUES (1, 1);
INSERT INTO `tn_auth_role_user` VALUES (2, 2);
INSERT INTO `tn_auth_role_user` VALUES (3, 1);
INSERT INTO `tn_auth_role_user` VALUES (4, 2);
COMMIT;

-- ----------------------------
-- Table structure for tn_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_rule`;
CREATE TABLE `tn_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限规则主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限规则父级id',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '权限规则标题',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '权限规则标识',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '规则验证方式，如果为1则可以验证condition中的规则',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '权限规则排序',
  `public_rule` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为公用权限（1 是 0 否）',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '权限状态（1 开启 0 关闭）',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '多重验证用到',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tn_auth_rule
-- ----------------------------
BEGIN;
INSERT INTO `tn_auth_rule` VALUES (1, 0, '管理员', 'admin', 1, 1, 0, 1, '', 1588659366, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (2, 1, '获取管理员列表数据', 'admin/getlist', 1, 1, 0, 1, '', 1588659441, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (3, 1, '根据id获取管理员信息', 'admin/getbyid', 1, 2, 0, 1, '', 1588659466, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (4, 1, '添加管理员', 'admin/addadmin', 1, 3, 0, 1, '', 1588667563, 1588668620, NULL);
INSERT INTO `tn_auth_rule` VALUES (5, 1, '编辑管理员', 'admin/editadmin', 1, 4, 0, 1, '', 1588667651, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (6, 1, '更新管理员信息', 'admin/upadteadmin', 1, 5, 0, 1, '', 1588667739, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (7, 1, '删除指定的管理员信息', 'admin/deleteadmin', 1, 6, 0, 1, '', 1588667833, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (8, 1, '管理员登陆', 'admin/login', 1, 7, 1, 1, '', 1588668922, 1588734790, NULL);
INSERT INTO `tn_auth_rule` VALUES (9, 1, '管理员退出', 'admin/logout', 1, 8, 1, 1, '', 1588668944, 1588668998, NULL);
INSERT INTO `tn_auth_rule` VALUES (10, 1, '获取对应管理员的路由信息', 'admin/routes', 1, 9, 1, 1, '', 1588669017, 1588669024, NULL);
INSERT INTO `tn_auth_rule` VALUES (11, 1, '获取对应管理员的信息', 'admin/info', 1, 10, 1, 1, '', 1588669163, 1588669166, NULL);
INSERT INTO `tn_auth_rule` VALUES (12, 0, '权限管理', 'auth', 1, 2, 0, 1, '', 1588669462, 1588669462, NULL);
INSERT INTO `tn_auth_rule` VALUES (13, 12, '权限角色', 'authrole', 1, 1, 0, 1, '', 1588669501, 1588669501, NULL);
INSERT INTO `tn_auth_rule` VALUES (14, 12, '权限菜单', 'authmenu', 1, 2, 0, 1, '', 1588669549, 1588669549, NULL);
INSERT INTO `tn_auth_rule` VALUES (15, 12, '权限规则', 'authrule', 1, 3, 0, 1, '', 1588669563, 1588669563, NULL);
INSERT INTO `tn_auth_rule` VALUES (16, 13, '获取权限角色列表数据', 'authrole/getlist', 1, 1, 0, 1, '', 1588669593, 1588669593, NULL);
INSERT INTO `tn_auth_rule` VALUES (17, 13, '获取权限角色的名称数据', 'authrole/getall', 1, 2, 0, 1, '', 1588669618, 1588827803, NULL);
INSERT INTO `tn_auth_rule` VALUES (18, 13, '根据id获取指定的数据', 'authrole/getbyid', 1, 3, 0, 1, '', 1588669704, 1588669704, NULL);
INSERT INTO `tn_auth_rule` VALUES (19, 13, '添加权限角色', 'authrole/addrole', 1, 4, 0, 1, '', 1588669723, 1588669723, NULL);
INSERT INTO `tn_auth_rule` VALUES (20, 13, '编辑权限角色', 'authrole/editrole', 1, 5, 0, 1, '', 1588669941, 1588669941, NULL);
INSERT INTO `tn_auth_rule` VALUES (21, 13, '更新权限角色信息', 'authrole/updaterole', 1, 6, 0, 1, '', 1588677433, 1588677433, NULL);
INSERT INTO `tn_auth_rule` VALUES (22, 13, '删除指定的权限信息', 'authrole/deleterole', 1, 7, 0, 1, '', 1588677457, 1588677457, NULL);
INSERT INTO `tn_auth_rule` VALUES (23, 14, '获取表格树形数据', 'authmenu/gettabletree', 1, 1, 0, 1, '', 1588677512, 1588677512, NULL);
INSERT INTO `tn_auth_rule` VALUES (24, 14, '根据id获取角色权限菜单的信息', 'authmenu/getbyid', 1, 2, 0, 1, '', 1588677542, 1588677542, NULL);
INSERT INTO `tn_auth_rule` VALUES (25, 14, '添加角色权限菜单', 'authmenu/addmenu', 1, 3, 0, 1, '', 1588677561, 1588677561, NULL);
INSERT INTO `tn_auth_rule` VALUES (26, 14, '编辑角色权限菜单', 'authmenu/editmenu', 1, 4, 0, 1, '', 1588677578, 1588677578, NULL);
INSERT INTO `tn_auth_rule` VALUES (27, 14, '更新角色权限菜单信息', 'authmenu/updatemenu', 1, 5, 0, 1, '', 1588677820, 1588677820, NULL);
INSERT INTO `tn_auth_rule` VALUES (28, 14, '删除角色权限菜单信息', 'authmenu/deletemenu', 1, 6, 0, 1, '', 1588677839, 1588677839, NULL);
INSERT INTO `tn_auth_rule` VALUES (29, 14, '获取角色权限菜单的所有节点信息', 'authmenu/getallmenunode', 1, 7, 0, 1, '', 1588677859, 1588677859, NULL);
INSERT INTO `tn_auth_rule` VALUES (30, 14, '获取指定父节点下的子节点数量', 'authmenu/getchildrencount', 1, 8, 0, 1, '', 1588677882, 1588677882, NULL);
INSERT INTO `tn_auth_rule` VALUES (31, 15, '获取表格树形数据', 'authrule/gettabletree', 1, 1, 0, 1, '', 1588678146, 1588678146, NULL);
INSERT INTO `tn_auth_rule` VALUES (32, 15, '根据id获取角色权限规则的信息', 'authrule/getbyid', 1, 2, 0, 1, '', 1588678483, 1588678483, NULL);
INSERT INTO `tn_auth_rule` VALUES (33, 15, '添加角色权限规则', 'authrule/addrule', 1, 3, 0, 1, '', 1588678597, 1588678597, NULL);
INSERT INTO `tn_auth_rule` VALUES (34, 15, '编辑角色权限规则', 'authrule/editrule', 1, 4, 0, 1, '', 1588678709, 1588678709, NULL);
INSERT INTO `tn_auth_rule` VALUES (35, 15, '更新角色权限规则信息', 'authrule/updaterule', 1, 5, 0, 1, '', 1588678730, 1588678730, NULL);
INSERT INTO `tn_auth_rule` VALUES (36, 15, '删除角色权限规则信息', 'authrule/deleterule', 1, 6, 0, 1, '', 1588678884, 1588678884, NULL);
INSERT INTO `tn_auth_rule` VALUES (37, 15, '获取角色权限规则的所有节点信息', 'authrule/getallrulenode', 1, 7, 0, 1, '', 1588678903, 1588678903, NULL);
INSERT INTO `tn_auth_rule` VALUES (38, 15, '获取指定父节点下的子节点数量', 'authrule/getchildrencount', 1, 8, 0, 1, '', 1588678923, 1588734805, NULL);
INSERT INTO `tn_auth_rule` VALUES (39, 14, '获取树形结构数据', 'authmenu/getelementtree', 1, 9, 0, 1, '', 1588852520, 1588852520, NULL);
INSERT INTO `tn_auth_rule` VALUES (40, 15, '获取树形结构数据', 'authrule/getelementtree', 1, 9, 0, 1, '', 1588852541, 1588852541, NULL);
INSERT INTO `tn_auth_rule` VALUES (41, 0, '系统', 'system', 1, 3, 0, 1, '', 1589423745, 1589423783, NULL);
INSERT INTO `tn_auth_rule` VALUES (42, 41, '系统设置', 'systemconfig', 1, 1, 0, 1, '', 1589423773, 1589423792, NULL);
INSERT INTO `tn_auth_rule` VALUES (43, 42, '获取表格树形数据', 'systemconfig/gettabletree', 1, 1, 0, 1, '', 1589423880, 1589423880, NULL);
INSERT INTO `tn_auth_rule` VALUES (44, 42, '根据id获取配置对应的信息', 'systemconfig/getbyid', 1, 2, 0, 1, '', 1589423899, 1589423899, NULL);
INSERT INTO `tn_auth_rule` VALUES (45, 42, '添加系统配置项信息', 'systemconfig/addconfig', 1, 3, 0, 1, '', 1589423922, 1589423922, NULL);
INSERT INTO `tn_auth_rule` VALUES (46, 42, '编辑系统配置项信息', 'systemconfig/editconfig', 1, 4, 0, 1, '', 1589423948, 1589423948, NULL);
INSERT INTO `tn_auth_rule` VALUES (47, 42, '更新系统配置项信息', 'systemconfig/updateconfig', 1, 5, 0, 1, '', 1589423974, 1589423974, NULL);
INSERT INTO `tn_auth_rule` VALUES (48, 42, '删除系统配置项信息', 'systemconfig/deleteconfig', 1, 6, 0, 1, '', 1589424000, 1589424000, NULL);
INSERT INTO `tn_auth_rule` VALUES (49, 42, '更新系统配置信息', 'systemconfig/commitconfigdata', 1, 7, 0, 1, '', 1589424023, 1589424023, NULL);
INSERT INTO `tn_auth_rule` VALUES (50, 42, '获取配置菜单信息', 'systemconfig/getmenudata', 1, 8, 0, 1, '', 1589424044, 1589424044, NULL);
INSERT INTO `tn_auth_rule` VALUES (51, 42, '获取系统配置的所有父节点信息', 'systemconfig/getallconfigparentnode', 1, 9, 0, 1, '', 1589424075, 1589424075, NULL);
INSERT INTO `tn_auth_rule` VALUES (52, 42, '获取指定父节点下的子节点数量', 'systemconfig/getchildrencount', 1, 10, 0, 1, '', 1589424117, 1589424117, NULL);
INSERT INTO `tn_auth_rule` VALUES (53, 41, '系统日志', 'systemlog', 1, 2, 0, 1, '', 1590208320, 1590208347, NULL);
INSERT INTO `tn_auth_rule` VALUES (54, 53, '获取日志列表信息', 'systemlog/getlist', 1, 1, 0, 1, '', 1590208691, 1590208691, NULL);
INSERT INTO `tn_auth_rule` VALUES (55, 53, '删除指定的日志信息', 'systemlog/deletelog', 1, 2, 0, 1, '', 1590208715, 1590208715, NULL);
INSERT INTO `tn_auth_rule` VALUES (56, 53, '清空日志信息', 'systemlog/clearalllog', 1, 3, 0, 1, '', 1590208737, 1590208737, NULL);
INSERT INTO `tn_auth_rule` VALUES (57, 0, '图集', 'Atlas', 1, 4, 0, 1, '', 1590208857, 1590212008, NULL);
INSERT INTO `tn_auth_rule` VALUES (58, 57, '图集栏目管理', 'atlascategory', 1, 1, 0, 1, '', 1590209372, 1590209372, NULL);
INSERT INTO `tn_auth_rule` VALUES (59, 58, '获取图集类目分类的分页数据', 'atlascategory/getlist', 1, 1, 0, 1, '', 1590211662, 1590211662, NULL);
INSERT INTO `tn_auth_rule` VALUES (60, 58, '根据id获取图集类目分类的数据', 'atlascategory/getbyid', 1, 2, 0, 1, '', 1590211816, 1590211816, NULL);
INSERT INTO `tn_auth_rule` VALUES (61, 58, '获取图集类目分类的全部名称信息', 'atlascategory/getallname', 1, 3, 1, 1, '', 1590211855, 1590211855, NULL);
INSERT INTO `tn_auth_rule` VALUES (62, 58, '添加图集类目分类信息', 'atlascategory/addatlascategory', 1, 4, 0, 1, '', 1590211888, 1590211888, NULL);
INSERT INTO `tn_auth_rule` VALUES (63, 58, '编辑图集类目分类信息', 'atlascategory/editatlascategory', 1, 5, 0, 1, '', 1590211919, 1590211919, NULL);
INSERT INTO `tn_auth_rule` VALUES (64, 58, '更新图集类目分类信息', 'atlascategory/updateatlascategory', 1, 6, 0, 1, '', 1590211956, 1590211956, NULL);
INSERT INTO `tn_auth_rule` VALUES (65, 58, '删除图集类目分类信息', 'atlascategory/deleteatalscategory', 1, 7, 0, 1, '', 1590211978, 1590211978, NULL);
INSERT INTO `tn_auth_rule` VALUES (66, 57, '图集管理', 'atlasmananger', 1, 2, 0, 1, '', 1590212040, 1590212040, NULL);
INSERT INTO `tn_auth_rule` VALUES (67, 66, '修改图集中图片的所属栏目', 'atlas/changecategory', 1, 1, 1, 1, '', 1590212181, 1590212181, NULL);
INSERT INTO `tn_auth_rule` VALUES (68, 0, '轮播', 'Banner', 1, 5, 0, 1, '', 1591597262, 1591597262, NULL);
INSERT INTO `tn_auth_rule` VALUES (69, 68, '轮播图', 'bannerimage', 1, 1, 0, 1, '', 1591597474, 1591597496, NULL);
INSERT INTO `tn_auth_rule` VALUES (70, 69, '获取轮播图分页数据', 'banner/getlist', 1, 1, 0, 1, '', 1591597521, 1591597521, NULL);
INSERT INTO `tn_auth_rule` VALUES (71, 69, '根据id获取对应的轮播图信息', 'banner/getbyid', 1, 2, 0, 1, '', 1591597539, 1591597539, NULL);
INSERT INTO `tn_auth_rule` VALUES (72, 69, '获取指定轮播位下的轮播图数量', 'banner/getbannercountbyposid', 1, 3, 0, 1, '', 1591597681, 1591597681, NULL);
INSERT INTO `tn_auth_rule` VALUES (73, 69, '添加轮播图信息', 'banner/addbanner', 1, 4, 0, 1, '', 1591597696, 1591597696, NULL);
INSERT INTO `tn_auth_rule` VALUES (74, 69, '编辑轮播图图信息', 'banner/editbanner', 1, 5, 0, 1, '', 1591598144, 1591598144, NULL);
INSERT INTO `tn_auth_rule` VALUES (75, 69, '更新轮播图信息', 'banner/updatebanner', 1, 6, 0, 1, '', 1591598161, 1591598161, NULL);
INSERT INTO `tn_auth_rule` VALUES (76, 69, '删除轮播图信息', 'banner/deletebanner', 1, 7, 0, 1, '', 1591598179, 1591598179, NULL);
INSERT INTO `tn_auth_rule` VALUES (77, 68, '轮播位', 'bannerpos', 1, 2, 0, 1, '', 1591598198, 1591598198, NULL);
INSERT INTO `tn_auth_rule` VALUES (78, 77, '获取轮播位分页数据', 'bannerpos/getlist', 1, 1, 0, 1, '', 1591598302, 1591598302, NULL);
INSERT INTO `tn_auth_rule` VALUES (79, 77, '根据id获取对应的轮播位信息', 'bannerpos/getbyid', 1, 2, 0, 1, '', 1591598319, 1591598319, NULL);
INSERT INTO `tn_auth_rule` VALUES (80, 77, '获取轮播位全部名称', 'bannerpos/getalltitle', 1, 3, 0, 1, '', 1591598337, 1591621507, NULL);
INSERT INTO `tn_auth_rule` VALUES (81, 77, '添加轮播位信息', 'bannerpos/addbannerpos', 1, 4, 0, 1, '', 1591598358, 1591598397, NULL);
INSERT INTO `tn_auth_rule` VALUES (82, 77, '编辑轮播位位信息', 'bannerpos/editbannerpos', 1, 5, 0, 1, '', 1591598387, 1591598387, NULL);
INSERT INTO `tn_auth_rule` VALUES (83, 77, '删除轮播位信息', 'bannerpos/deletebannerpod', 1, 6, 0, 1, '', 1591598414, 1591598414, NULL);
INSERT INTO `tn_auth_rule` VALUES (84, 0, '栏目', 'Category', 1, 6, 0, 1, '', 1591609105, 1591609105, NULL);
INSERT INTO `tn_auth_rule` VALUES (85, 84, '获取栏目Table树形数据', 'category/gettabletree', 1, 1, 0, 1, '', 1591609752, 1591609752, NULL);
INSERT INTO `tn_auth_rule` VALUES (86, 84, '获取全部顶级节点信息', 'category/gettopnode', 1, 2, 0, 1, '', 1591609777, 1591609777, NULL);
INSERT INTO `tn_auth_rule` VALUES (87, 84, '获取栏目全部栏目', 'category/getallnode', 1, 3, 0, 1, '', 1591609808, 1591621487, NULL);
INSERT INTO `tn_auth_rule` VALUES (88, 84, '获取对应栏目下的子栏目数量', 'category/getchildrencount', 1, 4, 0, 1, '', 1591609840, 1591609840, NULL);
INSERT INTO `tn_auth_rule` VALUES (89, 84, '根据id获取对应栏目信息', 'category/getbyid', 1, 5, 0, 1, '', 1591609909, 1591609909, NULL);
INSERT INTO `tn_auth_rule` VALUES (90, 84, '添加栏目信息', 'category/addcategory', 1, 6, 0, 1, '', 1591609926, 1591609926, NULL);
INSERT INTO `tn_auth_rule` VALUES (91, 84, '编辑栏目信息', 'category/editcategory', 1, 7, 0, 1, '', 1591610370, 1591610370, NULL);
INSERT INTO `tn_auth_rule` VALUES (92, 84, '更新栏目信息', 'category/updatecategory', 1, 8, 0, 1, '', 1591610389, 1591610389, NULL);
INSERT INTO `tn_auth_rule` VALUES (93, 84, '删除栏目信息', 'category/deletecategory', 1, 9, 0, 1, '', 1591610439, 1591610439, NULL);
INSERT INTO `tn_auth_rule` VALUES (94, 0, '业务', 'Business', 1, 7, 0, 1, '', 1591610573, 1591610573, NULL);
INSERT INTO `tn_auth_rule` VALUES (95, 94, '获取业务分页数据', 'business/getlist', 1, 1, 0, 1, '', 1591610723, 1591621344, NULL);
INSERT INTO `tn_auth_rule` VALUES (96, 94, '根据id获取业务信息', 'business/getbyid', 1, 2, 0, 1, '', 1591610800, 1591621336, NULL);
INSERT INTO `tn_auth_rule` VALUES (97, 94, '获取业务的指定用户信息', 'business/getoperationuser', 1, 3, 0, 1, '', 1591610818, 1591610818, NULL);
INSERT INTO `tn_auth_rule` VALUES (98, 94, '添加业务信息', 'business/addbusiness', 1, 4, 0, 1, '', 1591610846, 1591610846, NULL);
INSERT INTO `tn_auth_rule` VALUES (99, 94, '编辑业务信息', 'business/editbusiness', 1, 5, 0, 1, '', 1591620931, 1591620931, NULL);
INSERT INTO `tn_auth_rule` VALUES (100, 94, '更新业务信息', 'business/updatebusiness', 1, 6, 0, 1, '', 1591620946, 1591620946, NULL);
INSERT INTO `tn_auth_rule` VALUES (101, 94, '删除业务信息', 'business/deletebusiness', 1, 7, 0, 1, '', 1591620964, 1591620964, NULL);
INSERT INTO `tn_auth_rule` VALUES (102, 0, '模型', 'ModelTable', 1, 8, 0, 1, '', 1591620991, 1591620991, NULL);
INSERT INTO `tn_auth_rule` VALUES (103, 102, '获取模型分页数据', 'modeltable/getlist', 1, 1, 0, 1, '', 1591621254, 1591621306, NULL);
INSERT INTO `tn_auth_rule` VALUES (104, 102, '根据id获取模型信息数据', 'modeltable/getbyid', 1, 2, 0, 1, '', 1591621287, 1591621287, NULL);
INSERT INTO `tn_auth_rule` VALUES (105, 102, '获取全部模型名称', 'modeltable/getallname', 1, 3, 0, 1, '', 1591621545, 1591621545, NULL);
INSERT INTO `tn_auth_rule` VALUES (106, 102, '获取模型字段数据', 'modeltable/getfieldsdata', 1, 4, 0, 1, '', 1591621635, 1591621635, NULL);
INSERT INTO `tn_auth_rule` VALUES (107, 102, '添加模型信息', 'modeltable/addmodeltable', 1, 5, 0, 1, '', 1591621652, 1591621652, NULL);
INSERT INTO `tn_auth_rule` VALUES (108, 102, '编辑模型信息', 'modeltable/editmodeltable', 1, 6, 0, 1, '', 1591621668, 1591621668, NULL);
INSERT INTO `tn_auth_rule` VALUES (109, 102, '更新模型信息', 'modeltable/updatemodeltable', 1, 7, 0, 1, '', 1591621685, 1591621685, NULL);
INSERT INTO `tn_auth_rule` VALUES (110, 102, '删除指定的模型信息', 'modeltable/deletemodeltable', 1, 8, 0, 1, '', 1591621704, 1591621704, NULL);
INSERT INTO `tn_auth_rule` VALUES (111, 0, '内容', 'Content', 1, 9, 0, 1, '', 1591621785, 1591621785, NULL);
INSERT INTO `tn_auth_rule` VALUES (112, 111, '获取内容的分页数据', 'content/getlist', 1, 1, 0, 1, '', 1591621797, 1591621797, NULL);
INSERT INTO `tn_auth_rule` VALUES (113, 111, '获取内容的指定用户信息', 'content/getoperationuser', 1, 2, 0, 1, '', 1591621822, 1591621822, NULL);
INSERT INTO `tn_auth_rule` VALUES (114, 111, '根据id获取内容信息', 'content/getbyid', 1, 3, 0, 1, '', 1591621836, 1591621836, NULL);
INSERT INTO `tn_auth_rule` VALUES (115, 111, '获取指定栏目下内容的数量', 'content/getcategorychildrencount', 1, 4, 0, 1, '', 1591621870, 1591621870, NULL);
INSERT INTO `tn_auth_rule` VALUES (116, 111, '添加内容信息', 'content/addcontent', 1, 5, 0, 1, '', 1591622078, 1591622078, NULL);
INSERT INTO `tn_auth_rule` VALUES (117, 111, '编辑内容信息', 'content/editcontent', 1, 6, 0, 1, '', 1591622093, 1591622093, NULL);
INSERT INTO `tn_auth_rule` VALUES (118, 111, '更新内容信息', 'content/updatecontent', 1, 7, 0, 1, '', 1591622108, 1591622108, NULL);
INSERT INTO `tn_auth_rule` VALUES (119, 111, '删除指定的内容信息', 'content/deletecontent', 1, 8, 0, 1, '', 1591622124, 1591622124, NULL);
INSERT INTO `tn_auth_rule` VALUES (120, 0, '订单', 'Order', 1, 10, 0, 1, '', 1591622278, 1591622278, NULL);
INSERT INTO `tn_auth_rule` VALUES (121, 120, '获取订单分页数据信息', 'order/getlist', 1, 1, 0, 1, '', 1591622314, 1591622314, NULL);
INSERT INTO `tn_auth_rule` VALUES (122, 120, '根据id获取订单信息', 'order/getbyid', 1, 2, 0, 1, '', 1591622329, 1591622329, NULL);
INSERT INTO `tn_auth_rule` VALUES (123, 120, '发起订单退款信息', 'order/refundorder', 1, 3, 0, 1, '', 1591622346, 1591622346, NULL);
INSERT INTO `tn_auth_rule` VALUES (124, 120, '删除订单信息', 'order/deleteorder', 1, 4, 0, 1, '', 1591622359, 1591622359, NULL);
INSERT INTO `tn_auth_rule` VALUES (125, 0, '微信', 'WeChat', 1, 11, 0, 1, '', 1594084469, 1594084469, NULL);
INSERT INTO `tn_auth_rule` VALUES (126, 125, '用户管理', 'wechatuser', 1, 1, 0, 1, '', 1594086101, 1594086101, NULL);
INSERT INTO `tn_auth_rule` VALUES (127, 126, '获取授权微信用户的分页数据', 'wechatuser/getlist', 1, 1, 0, 1, '', 1594086138, 1594086138, NULL);
INSERT INTO `tn_auth_rule` VALUES (128, 126, '更新授权微信用户的相关信息', 'wechatuser/updatewechatuser', 1, 2, 0, 1, '', 1594086206, 1594086206, NULL);
INSERT INTO `tn_auth_rule` VALUES (129, 125, '微信设置', 'wechatconfig', 1, 2, 0, 1, '', 1594086314, 1594086314, NULL);
INSERT INTO `tn_auth_rule` VALUES (130, 129, '获取表格树形数据', 'wxconfig/gettabletree', 1, 1, 0, 1, '', 1594086329, 1594086329, NULL);
INSERT INTO `tn_auth_rule` VALUES (131, 129, '根据id获取配置对应的信息', 'wxconfig/getbyid', 1, 2, 0, 1, '', 1594086356, 1594086356, NULL);
INSERT INTO `tn_auth_rule` VALUES (132, 129, '获取配置菜单信息', 'wxconfig/getmenudata', 1, 3, 0, 1, '', 1594086393, 1594086393, NULL);
INSERT INTO `tn_auth_rule` VALUES (133, 129, '获取微信配置的所有父节点信息', 'wxconfig/getallconfigparentnode', 1, 4, 0, 1, '', 1594086431, 1594086431, NULL);
INSERT INTO `tn_auth_rule` VALUES (134, 129, '获取指定父节点下的子节点数量', 'wxconfig/getchildrencount', 1, 5, 0, 1, '', 1594086564, 1594086564, NULL);
INSERT INTO `tn_auth_rule` VALUES (135, 129, '添加微信配置项信息', 'wxconfig/addconfig', 1, 6, 0, 1, '', 1594086581, 1594086581, NULL);
INSERT INTO `tn_auth_rule` VALUES (136, 129, '编辑微信配置项信息', 'wxconfig/editconfig', 1, 7, 0, 1, '', 1594086597, 1594086597, NULL);
INSERT INTO `tn_auth_rule` VALUES (137, 129, '更新微信配置项信息', 'wxconfig/updateconfig', 1, 8, 0, 1, '', 1594086614, 1594086614, NULL);
INSERT INTO `tn_auth_rule` VALUES (138, 129, '删除微信配置项信息', 'wxconfig/deleteconfig', 1, 9, 0, 1, '', 1594086629, 1594086629, NULL);
INSERT INTO `tn_auth_rule` VALUES (139, 129, '更新微信配置信息', 'wxconfig/commitconfigdata', 1, 10, 0, 1, '', 1594086648, 1594086648, NULL);
INSERT INTO `tn_auth_rule` VALUES (140, 66, '上传图集列表的图片', 'atlas/uploadimage', 1, 2, 0, 1, '', 1595639973, 1595640020, NULL);
INSERT INTO `tn_auth_rule` VALUES (141, 66, '删除图集列表中的图片', 'atlas/deleteimage', 1, 3, 0, 1, '', 1595640010, 1595640010, NULL);
INSERT INTO `tn_auth_rule` VALUES (142, 41, '文件上传', 'uploadfile', 1, 3, 0, 1, '', 1595640540, 1595640552, NULL);
INSERT INTO `tn_auth_rule` VALUES (143, 142, '上传文件到本地', 'upload/uploadfile', 1, 1, 0, 1, '', 1595640570, 1595640570, NULL);
INSERT INTO `tn_auth_rule` VALUES (144, 142, '删除服务器指定的文件', 'upload/deleteuploadtempfile', 1, 2, 0, 1, '', 1595640610, 1595640610, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_banner
-- ----------------------------
DROP TABLE IF EXISTS `tn_banner`;
CREATE TABLE `tn_banner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '轮播图主键id',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '轮播图的标题',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '轮播图图片地址',
  `pos_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '轮播图所属轮播位',
  `article_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '轮播图所属文章id',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态（1显示 0不显示）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='保存轮播图信息';

-- ----------------------------
-- Records of tn_banner
-- ----------------------------
BEGIN;
INSERT INTO `tn_banner` VALUES (1, '首页轮播1', '/storage/uploads/img_list/96/00a3c9fce9978d9e767f2e2f90fc7b.png', 1, 0, 1, 1, 1600235718, 1600235718, NULL);
INSERT INTO `tn_banner` VALUES (2, '首页轮播2', '/storage/uploads/img_list/95/a5526255d95418b434964c52cd056d.jpeg', 1, 0, 2, 1, 1600235730, 1600235730, NULL);
INSERT INTO `tn_banner` VALUES (3, '首页轮播3', '/storage/uploads/img_list/96/dbcde963e2a4d86fd460b6e4bf1769.jpeg', 1, 0, 3, 1, 1600235743, 1600235743, NULL);
INSERT INTO `tn_banner` VALUES (4, '首页轮播4', '/storage/uploads/img_list/1d/f27f460d2c5c9c0272324a9a9e5718.jpeg', 1, 0, 4, 1, 1600235754, 1600235754, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_banner_pos
-- ----------------------------
DROP TABLE IF EXISTS `tn_banner_pos`;
CREATE TABLE `tn_banner_pos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '轮播位的主键id',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '轮播位的名称',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='保存轮播图位置的相关信息';

-- ----------------------------
-- Records of tn_banner_pos
-- ----------------------------
BEGIN;
INSERT INTO `tn_banner_pos` VALUES (1, '首页顶部', 1590401346, 1590401588, NULL);
INSERT INTO `tn_banner_pos` VALUES (2, '文章底部', 1590401583, 1590401592, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_business
-- ----------------------------
DROP TABLE IF EXISTS `tn_business`;
CREATE TABLE `tn_business` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '业务主键id',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '业务标题',
  `query_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '咨询次数',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='保存业务信息';

-- ----------------------------
-- Records of tn_business
-- ----------------------------
BEGIN;
INSERT INTO `tn_business` VALUES (1, 'UI设计', 0, 1, 1, 1578622937, 1600174617, NULL);
INSERT INTO `tn_business` VALUES (2, '微信小程序', 0, 2, 1, 1578623191, 1598581841, NULL);
INSERT INTO `tn_business` VALUES (3, '网站开发', 0, 3, 1, 1578623257, 1600174645, NULL);
INSERT INTO `tn_business` VALUES (4, '其他业务', 0, 4, 1, 1578623291, 1595477373, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_business_advisory_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_business_advisory_user`;
CREATE TABLE `tn_business_advisory_user` (
  `business_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned DEFAULT NULL,
  KEY `business_id` (`business_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `business_user_id` (`business_id`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存业务与咨询用户之间的关系';

-- ----------------------------
-- Records of tn_business_advisory_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_business_data
-- ----------------------------
DROP TABLE IF EXISTS `tn_business_data`;
CREATE TABLE `tn_business_data` (
  `title_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '对应业务流程标题id',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '内容主标题',
  `sub_title` varchar(60) NOT NULL DEFAULT '' COMMENT '内容次标题',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存业务流程下面的信息';

-- ----------------------------
-- Records of tn_business_data
-- ----------------------------
BEGIN;
INSERT INTO `tn_business_data` VALUES (93, '没有原型', '原型设计', '没有原型，也就是没有业务流程图，是画不了UI图的，请先提供完整业务原型，或前往沟通洽谈，原型付费设计100+每页（不谈价）');
INSERT INTO `tn_business_data` VALUES (93, '有原型', '沟通交流', '请提供表述完善的原型图，且需确定风格，主题颜色，配色要求，如难以表述，请提供相关案例图');
INSERT INTO `tn_business_data` VALUES (94, '设计时长', '确认时长', '设计时间周期我们会根据需求给予相关的设计时间，例如，业务流程原型图设计完善，且页面数量在20个内，设计时间为7个工作日（参考值）');
INSERT INTO `tn_business_data` VALUES (94, '已确认相关需求', '评估报价', '根据提供的UI设计需求，双方沟通讨论，并对UI设计需求的合理性和可行性分析，确定最终的UI设计需求，包括图标设计，UI设计200—500每页');
INSERT INTO `tn_business_data` VALUES (95, '微信支付', '定金金额', '设计需求确认后，客户需支付最终报价费用的50%作为定金，并提供邮箱');
INSERT INTO `tn_business_data` VALUES (96, 'UI设计', '首页稿确认', '收到定金后，根据最终的设计需求，首先进行首页的效果图设计，且将首页UI图发送客户初步确认');
INSERT INTO `tn_business_data` VALUES (97, '设计稿交付', '终稿确认', '终稿提供PSD格式，以iphonex为例，客户的前端工程师可将设计稿进行切图');
INSERT INTO `tn_business_data` VALUES (98, '微信支付', '尾款金额', '支付剩余定制设计费用（尾款），设计稿文件将发送邮箱');
INSERT INTO `tn_business_data` VALUES (99, '注意事项', '注意事项', '设计不支持额外的新增的功能的设计，且不支持风格的更改与重新设计，望须知');
INSERT INTO `tn_business_data` VALUES (100, '小程序类型', '确定类型', '首先确定需要设计开发的小程序类型，例如工具类、内容资讯类、生活服务类、游戏娱乐类、电商类');
INSERT INTO `tn_business_data` VALUES (100, '资质', '确定资质', '如果是个人资质，那么限制是很多的，有个体工商户营业执照，或者企业资质是最好的，同时有已认证好的公众号能够直接复用资质给小程序账号');
INSERT INTO `tn_business_data` VALUES (100, '服务器', '服务器', '如果你没有服务器，服务器可自行上阿里云或腾讯云购买，更或者我们协助你们购买（因为云服务器购买需要实名认证，而服务器我们只是替你们配置好并存放项目，也就是服务器是你们的，如果有需求，可代管理。\n阿里云建议使用支付宝账号进行注册然后绑定手机号，并且完成实名认证）');
INSERT INTO `tn_business_data` VALUES (101, '相关功能点', '评估报价', '根据功能点和相关实现难点，给予评估报价，实现包括“原型流程，UI设计，后台开发，前端开发，项目上线”，如果能提供原型流程图，那么是最好的');
INSERT INTO `tn_business_data` VALUES (101, '开发时长', '确定时长', '开发时长往往取决于小程序类型和相关功能点，一般小程序开发上线时长在2-3个月');
INSERT INTO `tn_business_data` VALUES (102, '定金支付', '定金金额', '小程序项目开始着手，客户需支付最终报价费用的50%作为定金，并以541形式收取（定金50%，上线40%，确认无误10%）');
INSERT INTO `tn_business_data` VALUES (102, '对公账户', '可开票', '如需开票，需提前说明，可开图鸟电子发票');
INSERT INTO `tn_business_data` VALUES (103, '小程序上线', '小程序', '项目正常上线');
INSERT INTO `tn_business_data` VALUES (104, '注意事项', '注意事项', '如果需要额外增加功能，那么需要再次评估并额外支付新功能款项');
INSERT INTO `tn_business_data` VALUES (105, '网站类型', '网站类型', '网站分企业官网、品牌网站、营销网站、电商网站、门户网站等等，我们主要开发企业官网，官网建设主要展示企业形象、企业产品或业务、新闻资讯、公司联系信息等。 企业网站建设的普遍意义是企业对外宣传交流的窗口');
INSERT INTO `tn_business_data` VALUES (105, '网站需求', '需求细节', '需要确定开发静态官网还是全套，如果是全套，可以上 http://www.tuniaoweb.com/anlizhanshi.html  ，图鸟web提供196套案例模板，例如【妙珮网站官网】就是开发案例之一');
INSERT INTO `tn_business_data` VALUES (105, '服务器', '服务器', '如果你没有服务器，服务器可自行上阿里云或腾讯云购买，更或者我们协助你们购买（因为云服务器购买需要实名认证，而服务器我们只是替你们配置好并存放项目，也就是服务器是你们的，如果有需求，可代管理。\n阿里云建议使用支付宝账号进行注册然后绑定手机号，并且完成实名认证）');
INSERT INTO `tn_business_data` VALUES (106, '定制官网', '定制官网', '如果是定制官网，需要提供风格，以及相关主色要求，同时提供相关内容资料，我们一般采用自适应开发，也就是一套网站，可以适应所有屏幕的显示。');
INSERT INTO `tn_business_data` VALUES (107, '相关功能点', '评估报价', '根据功能点和相关实现难点，给予评估报价，实现包括“原型流程，UI设计，后台开发，前端开发，项目上线”，如果能提供原型流程图，那么是最好的');
INSERT INTO `tn_business_data` VALUES (107, '开发时长', '确定时长', '开发时长往往取决于官网设计类型和相关功能点，一般官网开发上线时长在1个月内');
INSERT INTO `tn_business_data` VALUES (108, '支付定金', '定金金额', '项目开始着手，客户需支付最终报价费用的40%作为定金，并以46形式收取（定金40%，确认无误60%），可开发票');
INSERT INTO `tn_business_data` VALUES (109, '注意事项', '注意事项', '如果需要额外增加功能，那么需要再次评估并额外支付新功能款项');
INSERT INTO `tn_business_data` VALUES (110, '开发业务', '开发业务', '不属于以上三种业务之一，开发类型的业务，需求表达清晰，有相应预算可接');
INSERT INTO `tn_business_data` VALUES (111, '设计业务', '设计业务', '如图标设计，H5设计业务等');
COMMIT;

-- ----------------------------
-- Table structure for tn_business_title
-- ----------------------------
DROP TABLE IF EXISTS `tn_business_title`;
CREATE TABLE `tn_business_title` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '业务流程标题主键id',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '流程标题',
  `business_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '对应的业务id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COMMENT='保存业务流程的标题';

-- ----------------------------
-- Records of tn_business_title
-- ----------------------------
BEGIN;
INSERT INTO `tn_business_title` VALUES (93, '需求沟通', 1);
INSERT INTO `tn_business_title` VALUES (94, '需求评估', 1);
INSERT INTO `tn_business_title` VALUES (95, '支付定金', 1);
INSERT INTO `tn_business_title` VALUES (96, '相关设计', 1);
INSERT INTO `tn_business_title` VALUES (97, '终稿确认', 1);
INSERT INTO `tn_business_title` VALUES (98, '支付余款', 1);
INSERT INTO `tn_business_title` VALUES (99, '注意事项', 1);
INSERT INTO `tn_business_title` VALUES (100, '需求沟通', 2);
INSERT INTO `tn_business_title` VALUES (101, '需求评估', 2);
INSERT INTO `tn_business_title` VALUES (102, '付款模式', 2);
INSERT INTO `tn_business_title` VALUES (103, '项目上线', 2);
INSERT INTO `tn_business_title` VALUES (104, '注意事项', 2);
INSERT INTO `tn_business_title` VALUES (105, '需求沟通', 3);
INSERT INTO `tn_business_title` VALUES (106, '风格相关', 3);
INSERT INTO `tn_business_title` VALUES (107, '需求评估', 3);
INSERT INTO `tn_business_title` VALUES (108, '付款模式', 3);
INSERT INTO `tn_business_title` VALUES (109, '注意事项', 3);
INSERT INTO `tn_business_title` VALUES (110, '开发业务', 4);
INSERT INTO `tn_business_title` VALUES (111, '设计业务', 4);
COMMIT;

-- ----------------------------
-- Table structure for tn_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_category`;
CREATE TABLE `tn_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '栏目主键id',
  `pid` int(10) unsigned NOT NULL COMMENT '上级栏目id',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '栏目标题',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '栏目状态（1开启 0关闭）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='保存栏目信息';

-- ----------------------------
-- Records of tn_category
-- ----------------------------
BEGIN;
INSERT INTO `tn_category` VALUES (1, 0, '案例', 1, 1, 1577858261, 1590491275, NULL);
INSERT INTO `tn_category` VALUES (2, 0, '资讯', 2, 1, 1577858282, 1577858282, NULL);
INSERT INTO `tn_category` VALUES (3, 1, '小程序', 1, 1, 1577858312, 1590492159, NULL);
INSERT INTO `tn_category` VALUES (4, 1, '网站', 2, 1, 1577858324, 1577858324, NULL);
INSERT INTO `tn_category` VALUES (5, 1, '电商', 4, 1, 1577858339, 1583118738, NULL);
INSERT INTO `tn_category` VALUES (6, 1, 'UI设计', 3, 1, 1577858349, 1583118747, NULL);
INSERT INTO `tn_category` VALUES (7, 2, 'UI设计', 1, 1, 1577858377, 1577858377, NULL);
INSERT INTO `tn_category` VALUES (8, 2, '版本迭代', 2, 1, 1578619054, 1600159752, NULL);
INSERT INTO `tn_category` VALUES (9, 1, '其他', 5, 1, 1592881061, 1592881061, NULL);
INSERT INTO `tn_category` VALUES (10, 2, 'icon', 3, 1, 1592881395, 1592881395, NULL);
INSERT INTO `tn_category` VALUES (11, 2, '小程序', 4, 1, 1592885410, 1592885410, NULL);
INSERT INTO `tn_category` VALUES (12, 2, '开源', 5, 1, 1600159737, 1600159737, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_content
-- ----------------------------
DROP TABLE IF EXISTS `tn_content`;
CREATE TABLE `tn_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '内容主键id',
  `title` varchar(150) DEFAULT NULL COMMENT '内容对应的标题',
  `main_image` varchar(255) NOT NULL DEFAULT '' COMMENT '内容的主图',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '栏目id',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所使用模型的id',
  `table_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '对应模型表存放内容的id',
  `recomm` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为推荐内容（1推荐 0不推荐）',
  `view_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览次数',
  `like_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞次数',
  `share_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分享次数',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否为可读（1可读 0不可读）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存文章内容信息';

-- ----------------------------
-- Records of tn_content
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_content_like_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_content_like_user`;
CREATE TABLE `tn_content_like_user` (
  `content_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  UNIQUE KEY `content_user_id` (`content_id`,`user_id`) USING BTREE,
  KEY `content_id` (`content_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存内容点赞数与用户之间的关系';

-- ----------------------------
-- Records of tn_content_like_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_content_share_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_content_share_user`;
CREATE TABLE `tn_content_share_user` (
  `content_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  UNIQUE KEY `content_user_id` (`content_id`,`user_id`) USING BTREE,
  KEY `content_id` (`content_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存内容分享数与用户之间的关系';

-- ----------------------------
-- Records of tn_content_share_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_content_view_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_content_view_user`;
CREATE TABLE `tn_content_view_user` (
  `content_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  UNIQUE KEY `content_user_id` (`content_id`,`user_id`) USING BTREE,
  KEY `content_id` (`content_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存内容查看数与用户之间的关系';

-- ----------------------------
-- Records of tn_content_view_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_model_fields
-- ----------------------------
DROP TABLE IF EXISTS `tn_model_fields`;
CREATE TABLE `tn_model_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型字段主键id',
  `cn_name` varchar(60) NOT NULL DEFAULT '' COMMENT '模型字段中文名',
  `en_name` varchar(60) NOT NULL DEFAULT '' COMMENT '模型字段英文名',
  `values` varchar(255) NOT NULL DEFAULT '' COMMENT '模型字段的可选值',
  `tips` varchar(255) NOT NULL DEFAULT '' COMMENT '模型字段的提示信息',
  `type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '模型字段的类型',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型字段所属模型',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `model_id` (`model_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='保存模型对应字段信息';

-- ----------------------------
-- Records of tn_model_fields
-- ----------------------------
BEGIN;
INSERT INTO `tn_model_fields` VALUES (1, '关键词', 'keywords', '', '请输入关键词', 5, 1, 1);
INSERT INTO `tn_model_fields` VALUES (2, '描述', 'desc', '', '请输入描述', 5, 1, 2);
INSERT INTO `tn_model_fields` VALUES (3, '内容', 'content', '', '', 6, 1, 3);
INSERT INTO `tn_model_fields` VALUES (4, '关键词', 'keywords', '', '请输入关键词', 5, 2, 1);
INSERT INTO `tn_model_fields` VALUES (5, '描述', 'desc', '', '请输入描述', 5, 2, 2);
INSERT INTO `tn_model_fields` VALUES (6, '内容', 'content', '', '', 6, 2, 3);
COMMIT;

-- ----------------------------
-- Table structure for tn_model_table
-- ----------------------------
DROP TABLE IF EXISTS `tn_model_table`;
CREATE TABLE `tn_model_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型主键id',
  `cn_name` varchar(60) NOT NULL DEFAULT '' COMMENT '模型中文名称',
  `en_name` varchar(60) NOT NULL DEFAULT '' COMMENT '模型英文名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态 （1 开启 0 关闭）',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `enname` (`en_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='保存模型与模型表之间的信息';

-- ----------------------------
-- Records of tn_model_table
-- ----------------------------
BEGIN;
INSERT INTO `tn_model_table` VALUES (1, '案例模型', 'website_case', 1, 1591068338, 1591068338, NULL);
INSERT INTO `tn_model_table` VALUES (2, '资讯模型', 'website_information', 1, 1591068415, 1591068415, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_system_config
-- ----------------------------
DROP TABLE IF EXISTS `tn_system_config`;
CREATE TABLE `tn_system_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '设置的主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '设置的父级id',
  `cn_name` varchar(80) NOT NULL DEFAULT '' COMMENT '配置对应的中文名',
  `en_name` varchar(80) NOT NULL DEFAULT '' COMMENT '配置对应的字段名称',
  `values` text NOT NULL COMMENT '配置的可选值',
  `tips` varchar(255) NOT NULL COMMENT '配置提示值',
  `value` text NOT NULL COMMENT '配置的值',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '配置的类型 1：文本输入框 2：单选按钮 3：复选框 4：下拉菜单 5：文本域 6：富文本 7：单图片 8：多图片 9：附件上传',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '配置的排序序号',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '配置项的状态（1 开启 0 关闭）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_en_name` (`id`,`en_name`) USING BTREE,
  KEY `en_name` (`en_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='保存系统配置的相关信息';

-- ----------------------------
-- Records of tn_system_config
-- ----------------------------
BEGIN;
INSERT INTO `tn_system_config` VALUES (1, 0, '网站设置', '', '', '', '', 0, 1, 1, 1588944799, 1588944799, NULL);
INSERT INTO `tn_system_config` VALUES (2, 1, '标题', 'site_title', '', '', '图鸟科技小程序官网后台管理系统', 1, 1, 1, 1589362296, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (3, 1, '域名', 'site_url', '', '请以http://或者https://开头', 'http://website_open.tuniaokj.com/', 1, 2, 1, 1589362366, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (4, 1, '作者', 'site_author', '', '', '图鸟科技', 1, 3, 1, 1589367023, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (5, 1, '备案号', 'site_record', '', '', '', 1, 4, 1, 1589367043, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (6, 1, 'token缓存时间', 'site_token_expire_in', '', '设置token的缓存时间，以秒作为单位。提示：通常根据实际情况进行设置', '7200', 10, 5, 1, 1589367104, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (7, 1, '允许上传的最大文件大小', 'site_file_max_size', '', '设置上传文件的最大大小，以KB为单位。提示：1 M = 1024 KB', '2048', 10, 6, 1, 1589367156, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (8, 1, '开启https', 'site_https_on', '开启,关闭', '提示：此选项是开启或者关闭与微信服务器通讯的时候是否开启https(在Linus下部署请选中该选项)', '关闭', 2, 7, 1, 1589367240, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (9, 1, 'Logo', 'site_logo', '', '', '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', 7, 8, 1, 1589367265, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (10, 0, '后台设置', '', '', '', '', 0, 2, 1, 1589367318, 1589367318, NULL);
INSERT INTO `tn_system_config` VALUES (11, 10, '权限验证', 'bk_auth_on', '开启,关闭', '', '开启', 2, 1, 1, 1589367396, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (12, 10, '日志记录', 'bk_log_on', '开启,关闭', '', '关闭', 2, 2, 1, 1589367443, 1600235573, NULL);
INSERT INTO `tn_system_config` VALUES (13, 10, '多图片', 'multi_pic', '', '', '[]', 8, 3, 1, 1589720725, 1600235573, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tn_system_log
-- ----------------------------
DROP TABLE IF EXISTS `tn_system_log`;
CREATE TABLE `tn_system_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '系统日志的主键id',
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '操作的用户名',
  `controller` varchar(150) NOT NULL DEFAULT '' COMMENT '操作的控制器',
  `method` varchar(150) NOT NULL DEFAULT '' COMMENT '操作的方法',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '日志信息',
  `ip_address` varchar(60) NOT NULL DEFAULT '' COMMENT '操作的ip地址',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存系统操作日志';

-- ----------------------------
-- Records of tn_system_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_we_chat_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_we_chat_user`;
CREATE TABLE `tn_we_chat_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '登录用户表主键id',
  `openid` varchar(100) NOT NULL DEFAULT '' COMMENT '微信用户的open_id',
  `nick_name` varchar(100) NOT NULL DEFAULT '' COMMENT '用户的名称',
  `avatar_url` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户性别 (0未知 1男 2女)',
  `phone_number` varchar(20) NOT NULL DEFAULT '' COMMENT '用户电话号码',
  `from` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户的登录方式（1 微信小程序）',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '用户的状态 (0关闭 1开启)',
  `third_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '第三方功能授权状态（0 没授权 1已授权）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `openid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存登录用户的状态';

-- ----------------------------
-- Records of tn_we_chat_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_website_case
-- ----------------------------
DROP TABLE IF EXISTS `tn_website_case`;
CREATE TABLE `tn_website_case` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keywords` text NOT NULL COMMENT '关键词',
  `desc` text NOT NULL COMMENT '描述',
  `content` longtext NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存案例模型的信息';

-- ----------------------------
-- Records of tn_website_case
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_website_information
-- ----------------------------
DROP TABLE IF EXISTS `tn_website_information`;
CREATE TABLE `tn_website_information` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keywords` text NOT NULL COMMENT '关键词',
  `desc` text NOT NULL COMMENT '描述',
  `content` longtext NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='保存资讯模型的信息';

-- ----------------------------
-- Records of tn_website_information
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tn_wx_config
-- ----------------------------
DROP TABLE IF EXISTS `tn_wx_config`;
CREATE TABLE `tn_wx_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '设置的主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '设置的父级id',
  `cn_name` varchar(80) NOT NULL DEFAULT '' COMMENT '配置对应的中文名',
  `en_name` varchar(80) NOT NULL DEFAULT '' COMMENT '配置对应的字段名称',
  `values` text NOT NULL COMMENT '配置的可选值',
  `tips` varchar(255) NOT NULL COMMENT '配置提示值',
  `value` text NOT NULL COMMENT '配置的值',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '配置的类型 1：文本输入框 2：单选按钮 3：复选框 4：下拉菜单 5：文本域 6：富文本 7：单图片 8：多图片 9：附件上传',
  `sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '配置的排序序号',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '配置项的状态（1 开启 0 关闭）',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_en_name` (`id`,`en_name`) USING BTREE,
  KEY `en_name` (`en_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='保存系统配置的相关信息';

-- ----------------------------
-- Records of tn_wx_config
-- ----------------------------
BEGIN;
INSERT INTO `tn_wx_config` VALUES (1, 0, '小程序基础设置', '', '', '', '', 0, 1, 1, 1590383845, 1590383845, NULL);
INSERT INTO `tn_wx_config` VALUES (2, 1, 'APP_ID', 'mp_app_id', '', '请输入开发者的APP_ID', '', 1, 1, 1, 1590383974, 1600236485, NULL);
INSERT INTO `tn_wx_config` VALUES (3, 1, 'APP_SECRET', 'mp_app_secret', '', '请输入开发者APP_SECRET', '', 1, 2, 1, 1590384800, 1600236486, NULL);
INSERT INTO `tn_wx_config` VALUES (4, 1, '用户认证', 'mp_auth_user', '开启,关闭', '是否开启用户认证功能', '关闭', 2, 3, 1, 1590384902, 1600236486, NULL);
INSERT INTO `tn_wx_config` VALUES (5, 0, '小程序图片设置', '', '', '', '', 0, 2, 1, 1590384928, 1590384928, NULL);
INSERT INTO `tn_wx_config` VALUES (6, 5, '个人背景图', 'mp_person_bg', '', '请选择个人页面的背景图片', '/storage/uploads/img_list/63/3896e9d9cff93d354ee6c1419e6516.png', 7, 1, 1, 1590384979, 1600236486, NULL);
INSERT INTO `tn_wx_config` VALUES (7, 5, '首页Logo图', 'mp_index_logo', '', '上传首页左上角logo图片', '/storage/uploads/img_list/0c/2683c1e4d721f44aa466869a4c9d11.jpg', 7, 2, 1, 1590385012, 1600236486, NULL);
INSERT INTO `tn_wx_config` VALUES (8, 5, '关于我们背景', 'mp_about_bg', '', '请选择关于我们的背景图片', '/storage/uploads/img_list/58/48db7300add3602fc43156d305aae8.png', 7, 3, 1, 1590385055, 1600236486, NULL);
INSERT INTO `tn_wx_config` VALUES (9, 0, '小程序页面设置', '', '', '', '', 0, 3, 1, 1590385108, 1590385108, NULL);
INSERT INTO `tn_wx_config` VALUES (10, 9, '搜索框提示', 'mp_search_input_tips', '', '搜索框的提示信息', '搜索‘广州图鸟科技有限公司’', 1, 1, 1, 1590385174, 1600236486, NULL);
INSERT INTO `tn_wx_config` VALUES (11, 9, '默认搜索历史关键词', 'mp_default_search_keyword', '', '关键词之间用英文逗号隔开(,)', '图鸟,图鸟科技', 5, 2, 1, 1590385211, 1600236486, NULL);
INSERT INTO `tn_wx_config` VALUES (12, 1, '开启第三方认证', 'mp_auth_third', '开启,关闭', '是否开启第三方认证', '关闭', 2, 4, 1, 1590464707, 1600236486, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
