<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-11
 * Time: 13:30
 */

namespace app\common\exception;


class UrlNoPresenceException extends BaseException
{
    public $code = 404;
    public $msg = '当前访问地址不存在';
    public $errorCode = 10001;
}