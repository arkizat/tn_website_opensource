<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-11
 * Time: 09:39
 */

namespace app\common\exception;


class IllegalAccessException extends BaseException
{
    public $code = 401;
    public $msg = '非法访问，网站未授权';
    public $errorCode = 10100;
}