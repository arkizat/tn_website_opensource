<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-18
 * Time: 16:12
 */

namespace app\common\controller;


use app\BaseController;
use app\common\model\Upload as UploadModel;
use app\common\model\Atlas as AtlasModel;

class Upload extends BaseController
{
    /**
     * 上传文件到本地
     * @http POST
     * @url /upload/post/file
     * @return \think\response\Json
     */
    public function uploadFile()
    {
        $this->checkPostUrl();

        $type = $this->request->post('type','file');
        $dir_name = $this->request->post('dir_name','');

        // 获取上传的文件
        $file = $this->request->file('file');

        $result = UploadModel::uploadFile($file,$dir_name,$type);

        return !empty($result) ? tn_yes('上传文件成功',['data' => $result]) : tn_no('上传文件失败');
    }

    /**
     * 删除指定上传的临时文件
     * @http delete
     * @url /upload/delete/file
     * @return \think\response\Json
     */
    public function deleteUploadTempFile()
    {
        $this->checkDeleteUrl();

        $file_name = $this->request->delete('file_name','');

        $result = UploadModel::deleteUploadTempFileFile($file_name);

        return $result ? tn_yes('删除文件成功') : tn_no('删除文件失败');
    }
}