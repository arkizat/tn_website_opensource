<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-03
 * Time: 16:25
 */

namespace app\common\model\traits;


use app\common\exception\ContentException;
use app\admin\model\ModelTable as ModelTableModel;

trait InformationContent
{
    /**
     * 获取指定数量的资讯推荐数据
     * @param $limit
     * @param int $exceptID 除外id
     * @param bool $throwRecommEmpty 是否抛出为空异常
     * @return mixed
     */
    public static function getInformationRecommData($limit, $exceptID = 0, $throwRecommEmpty = true)
    {
        // 首先获取案例下所有的子栏目id
        $categoryAllID = static::getAllCategoryIDByContentType(static::information_category_id,'推荐的内容为空',20001);

        // 查找对应栏目id下指定数量的推荐内容
        $recommContent = static::with(['category'])
            ->where([['category_id','in',$categoryAllID],['id','<>',$exceptID],['recomm','=',1],['status','=',1]])
            ->withAttr('table_data',function ($value,$data){
                return static::getTableData($data,['desc']);
            })
            ->field(['id','title','main_image','model_id','table_id','category_id','view_count','like_count'])
            ->order(['sort'=>'ASC','create_time'=>'DESC'])
            ->limit($limit)
            ->select();

        if ($recommContent->isEmpty()) {
            if ($throwRecommEmpty) {
                throw new ContentException([
                    'msg' => '推荐的内容为空',
                    'errorCode' => 20001,
                ]);
            } else {
                return [];
            }

        }

        $recommContent->append(['table_data','view_count','like_count'])
            ->visible(['id','title','main_image']);

        return $recommContent;
    }

    /**
     * 获取指定数量的资讯最新数据
     * @param $limit
     * @return mixed
     */
    public static function getInformationNewestData($limit)
    {
        $categoryAllID = static::getAllCategoryIDByContentType(static::information_category_id,'最新的内容为空',40102);

        // 查找对应栏目id下指定数量的推荐内容
        $newestContent = static::with(['category'])
            ->where([['category_id','in',$categoryAllID],['status','=',1]])
            ->withAttr('table_data',function ($value,$data){
                return static::getTableData($data,['keywords']);
            })
            ->field(['id','title','main_image','model_id','table_id','category_id','view_count','like_count'])
            ->order(['create_time'=>'DESC'])
            ->limit($limit)
            ->select();

        if ($newestContent->isEmpty()) {
            throw new ContentException([
                'msg' => '最新的内容为空',
                'errorCode' => 40102,
            ]);
        }

        $newestContent->append(['table_data','view_count','like_count'])
            ->visible(['id','title','main_image']);

        return $newestContent;
    }

    /**
     * 获取栏目对应内容的分页数据
     * @param $params
     * @return mixed
     */
    public static function getInformationCategoryPaginate($params)
    {
        $categoryID = [];
        $categoryAllID = static::getAllCategoryIDByContentType(static::information_category_id,'当前栏目下内容为空',40103);

        // 判断是获取指定栏目还是全部的栏目下的数据
        if ($params['category_id'] == 0) {
            $categoryID = $categoryAllID;
        } else {
            if (in_array($params['category_id'],$categoryAllID) !== false) {
                $categoryID[] = $params['category_id'];
            } else {
                throw new ContentException([
                    'msg' => '当前栏目下内容为空',
                    'errorCode' => 40103,
                ]);
            }

        }

        // 获取当前内容对应模型的名字
        $modelID = static::where([['category_id','in',$categoryID],['status','=',1]])
            ->field(['model_id'])
            ->find();
        $modelName = ModelTableModel::getTableEnnameByID($modelID['model_id']);
        if (empty($modelName)) {
            throw new ContentException([
                'msg' => '当前栏目下内容为空',
                'errorCode' => 40103,
            ]);

        }

        $paginateData = static::with(['category'])
            ->alias('c')
            ->join($modelName . ' m','c.table_id = m.id')
            ->where([['c.category_id','in',$categoryID],['c.status','=',1]])
            ->field(['c.id','c.title','c.main_image','c.table_id','c.category_id','c.view_count','c.like_count','m.id'=>'mid','m.desc'])
            ->order(['create_time'=>'DESC'])
            ->paginate([
                'page' => $params['page'],
                'list_rows' => $params['limit']
            ],true);

        if ($paginateData->isEmpty()){
            throw new ContentException([
                'msg' => '当前栏目下内容为空',
                'errorCode' => 40103,
            ]);
        }

        $paginateData->visible(['id','title','desc','view_count','like_count','main_image']);

        return $paginateData;
    }

    /**
     * 获取资讯的详细信息
     * @param $id
     * @return mixed
     */
    public static function getInformationContentData($id)
    {
        $categoryAllID = static::getAllCategoryIDByContentType(static::information_category_id,'对应id的内容为空',20000);

        // 根据id获取对应的基本内容
        $contentData = static::with(['contentLikeUser.user'=>function($query){
            $query->limit(4)->order(['create_time'=>'DESC'])->field(['content_id','user_id']);
        },'category'])
            ->where([['id','=',$id],['category_id','in',$categoryAllID],['status','=',1]])
            ->field(['id','title','main_image','model_id','table_id','category_id','view_count','like_count','share_count'])
            ->withAttr('table_data',function ($value,$data){
                return static::getTableData($data);
            })
            ->withAttr('recomm_data',function ($value,$data) use ($id){
                return static::getInformationRecommData(2,$id,false);
            })
            ->find();

        if (empty($contentData)) {
            throw new ContentException();
        }

        $contentData->append(['table_data','recomm_data']);

        return $contentData;
    }
}