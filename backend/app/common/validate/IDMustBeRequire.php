<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-15
 * Time: 21:58
 */

namespace app\common\validate;


class IDMustBeRequire extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
    ];
}