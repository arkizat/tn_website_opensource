<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-02
 * Time: 20:18
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\model\Banner as BannerModel;

class Banner extends BaseController
{
    /**
     * 获取对应轮播位对应的轮播图
     * @http GET
     * @url /banner/:id/:limit
     * :id 对应轮播位的id
     * :limit 获取轮播图的数量
     * @return \think\response\Json
     */
    public function getBanner()
    {
        // 通过路由传递的参数需要使用param获取
        $params = $this->request->param(['pos_id','limit']);

        $banner = BannerModel::getBannerByPosID($params);

        return tn_yes('获取轮播图数据成功', ['data' => $banner]);
    }
}