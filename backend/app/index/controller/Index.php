<?php
declare (strict_types = 1);

namespace app\index\controller;

class Index
{
    public function index()
    {
        return '您好！欢迎使用图鸟官网开源版';
    }
}
