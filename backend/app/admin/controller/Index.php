<?php
declare (strict_types = 1);

namespace app\admin\controller;

class Index
{
    public function index()
    {
        return '';
    }

    /**
     * 获取系统的默认配置信息
     * @http get
     * @url /index/default_config
     * @return \think\response\Json
     */
    public function getDefaultConfig()
    {
        $data = [
            'site_title' => get_system_config('site_title')
        ];
        return tn_yes('获取默认配置信息成功',['data' => $data]);
    }
}
