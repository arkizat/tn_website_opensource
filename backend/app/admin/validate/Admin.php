<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-09
 * Time: 16:17
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class Admin extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'user_name' => 'require|alphaDash|max:20|unique:admin',
        'password' => 'require',
        'checkpass' => 'require|confirm:password',
        'nick_name' => 'require|max:20',
        'gender' => 'require|between:0,1',
        'avatar' => 'max:255',
        'introduction' => 'max:255',
        'role_id' => 'require|gt:0',
        'status' => 'require|number|between:0,1',
    ];

    protected $message = [
        'username.require' => '登录账号不能为空',
        'username.alphaDash' => '登录账号只能为字母、数字、_、-',
        'username.max' => '登录账号最大长度为20',
        'password.require' => '密码不能为空',
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'user_name.require' => '登录账号不能为空',
        'user_name.alphaDash' => '登录账号只能为字母、数字、_、-',
        'user_name.max' => '登录账号最大长度为20',
        'user_name.unique' => '登录账号不能重复',
        'nick_name.require' => '中文名称不能为空',
        'nick_name.max' => '中文名称最大长度为20',
        'checkpass.require' => '重复密码不能为空',
        'checkpass.confirm' => '两次输入的密码不一致',
        'gender.require' => '性别不能为空',
        'gender.between' => '性别请保持正常值',
        'avatar.max' => '头像地址最大长度不能超过255',
        'introduction.max' => '管理员简介长度不能超过255',
        'role_id.require' => '所属权限组不能为空',
        'role_id.gt' => '请选择正确的所属权限组',
        'status.require' => '状态不能为空',
        'status.number' => '状态值格式错误',
        'status.between' => '状态请保持正常值',
    ];

    protected $scene = [
        'add' => ['user_name','nick_name','password','checkpass','gender','avatar','introduction','status'],
        'edit' => ['id','user_name','nick_name','password','checkpass','gender','avatar','introduction','status'],
        'edit_no_password' => ['id','user_name','nick_name','gender','avatar','introduction','status']
    ];

    public function sceneLogin()
    {
        return $this->only(['user_name','password'])
            ->remove('user_name','unique');
    }
}